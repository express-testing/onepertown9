/**
 * v 1.0
 */
var cookiesManager = function(options) {
	var self = this;

    // UI settings
    self.defaults = {
        cookie_name: 'privacy_settings',
        open_settings_at_startup: true,
        necessary_cookies: 
        {
            name: 'Necessary Cookies',
            description: '<p>Necessary cookies enable core functionality such as page navigation and access to secure areas. The website cannot function properly without these cookies, and can only be disabled by changing your browser preferences.</p>'
        },
        cookies_groups: [
            {
                name: 'Analytics',
                description: '<p>The cookies we use are “analytical” cookies. They allow us to recognise and count the number of visitors and to see how visitors move around the site when they are using it. This helps us to improve the way our website works, for example by ensuring that users are finding what they are looking for easily.</p>',
                cookies: ['_ga']
            }
        ],
        bar: {
            text: '<h3>Your choice regarding cookies on this site</h3><p>We use cookies to optimise site functionality and give you the best possible experience.</p>',
            accept_text: 'I Accept',
            decline_text: 'I Do Not Accept',
            settings_text: 'Settings',
            classes: '',
            position: 'bottom'
        },
        settings: {
            heading: 'YOUR CHOICE REGARDING COOKIES ON THIS SITE',
            description: '<p>Our website uses cookies. A cookie is a small file of letters and numbers that we put on your computer if you agree. These cookies allow us to distinguish you from other users of our Website, which helps us to provide you with a good experience when you browse our Website and also allows us to improve our Website.</p>',
            accept_text: 'Accept',
            decline_text: 'Decline',
            read_more_text: '<p>Read more about the individual cookies we use and how to</p>'
        }
    };
    self.settings = Object.assign({}, self.defaults, options);

    // User settings
    var cookies_user_data = flash.readCookie(self.settings.cookie_name);
    if(cookies_user_data) {
        self.user_settings = JSON.parse(cookies_user_data);
    } else {
        self.user_settings = {groups:{}};
        self.settings.cookies_groups.forEach(function(group){
            self.user_settings.groups[group.name] = false;
        });
    }

    // Getting browser cookies property definition
    self.cookies_desc = Object.getOwnPropertyDescriptor(Document.prototype, 'cookie') || Object.getOwnPropertyDescriptor(HTMLDocument.prototype, 'cookie');

    // Init
    var cookies_functions_interval = setInterval(function() {
    	if(typeof flash.createCookie === 'function') {
    		clearInterval(cookies_functions_interval);
    		self.init();
    	}
    }, 30);

    return self;
}

/**
 * Init the interface
 */
cookiesManager.prototype.init = function() {
	var self = this;

    // COOKIE BAR
    // ===================
	if(!flash.readCookie(self.settings.cookie_name) && !self.settings.open_settings_at_startup) {
		var content = '<div class="cookies-bar ' + self.settings.bar.classes +'">' +
			'<div class="container cookies-bar__container">' +
                '<div class="cookies-bar__text">' + 
                    self.settings.bar.text +
                '</div>' +
                '<div class="cookies-bar__buttons">' +
                    '<button class="cookies-bar__button cookies-bar__button--accept" data-accept-cookies>' + self.settings.bar.accept_text + '</button>' +
                    '<button class="cookies-bar__button cookies-bar__button--decline" data-decline-cookies>' + self.settings.bar.decline_text + '</button>' +
                    '<button class="cookies-bar__button cookies-bar__button--settings" data-settings-cookies>' + self.settings.bar.settings_text + '</button>' +
                '</div>' +
                '<button class="cookies-bar__close-icon" data-decline-cookies></button>' +
			'</div>' +
        '</div>';
        
        // Appending the bar to the body
        if(self.settings.bar.position === 'bottom') {
            document.body.insertAdjacentHTML('beforeend', content);
        } else {
            document.body.insertAdjacentHTML('afterbegin', content);
        }

		// Getting direct reference to elements
    	self.bar = document.querySelector('.cookies-bar');
    }
    
    // SETTINGS INTERFACE
    // ===================
    var cookies_settings_ui =  '<div class="cookies-settings {class}">' +
                                    '<span class="cookies-settings__close-icon" data-close-settings-cookies></span>' + 
                                    '<div class="cookies-settings__inner">' +
                                        '<div class="cookies-settings__content">' +
                                            '<div class="cookies-settings__intro">' +
                                                '<h3 class="cookies-settings__heading">{heading}</h3>' +
                                                '<div class="wysiwyg cookies-settings__description">{description}</div>' +      
                                                '<div class="cookies-settings__buttons">' +
                                                    '<button class="cookies-settings__button cookies-settings__button--accept" data-accept-cookies>{accept_text}</button>' +
                                                    '<button class="cookies-settings__button cookies-settings__button--decline" data-decline-cookies>{decline_text}</button>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="cookies-settings__groups">' +
                                                '{groups}' +
                                            '</div>' + 
                                            '<div class="wysiwyg cookies-settings__read-more">{read_more}</div>' +     
                                        '</div>' + 
                                    '</div>' + 
                                '</div>' + 
                                '<div class="cookies-settings-opener" data-settings-cookies></div>';

    cookies_settings_ui = cookies_settings_ui.replace('{heading}', self.settings.settings.heading);
    cookies_settings_ui = cookies_settings_ui.replace('{description}', self.settings.settings.description);
    cookies_settings_ui = cookies_settings_ui.replace('{accept_text}', self.settings.settings.accept_text);
    cookies_settings_ui = cookies_settings_ui.replace('{decline_text}', self.settings.settings.decline_text);
    cookies_settings_ui = cookies_settings_ui.replace('{read_more}', self.settings.settings.read_more_text);
    
    var groups = '<div class="cookies-group cookies-group--necessary">' + 
        '<h4 class="cookies-group__name">' + self.settings.necessary_cookies.name + '</h4>' +
        '<div class="wysiwyg cookies-group__description">' + self.settings.necessary_cookies.description + '</div>' +
    '</div>';

    self.settings.cookies_groups.forEach(function(group) {
        groups +=   '<div class="cookies-group">' + 
                        '<h4 class="cookies-group__name">' + group.name + '</h4>' +
                        '<div class="wysiwyg cookies-group__description">' + group.description + '</div>' +
                        '<div class="cookies-group__toggle" data-toggle-group="' + group.name + '" data-toggle-group-status="' + self.user_settings.groups[group.name] + '"><span class="cookies-group__toggle-option">On</span><span class="cookies-group__toggle-option">Off</span></div>' +
                    '</div>';
    });

    cookies_settings_ui = cookies_settings_ui.replace('{groups}', groups);

    if(flash.readCookie(self.settings.cookie_name) || !self.settings.open_settings_at_startup) {
        cookies_settings_ui = cookies_settings_ui.replace('{class}', ' cookies-settings--hidden');
    } else {
        cookies_settings_ui = cookies_settings_ui.replace('{class}', '');
    }
    
    // Appending the bar to the body
    document.body.insertAdjacentHTML('beforeend', cookies_settings_ui);

    flash.listen('[data-toggle-group]', 'click', function(){
        self.toggleGroup(this.getAttribute('data-toggle-group'));
    }); 
    
    // Getting direct reference to the items of the settings ui
    self.settings_opener = document.querySelector('.cookies-settings-opener');
    self.settings_panel  = document.querySelector('.cookies-settings');

    // BUTTONS EVENTS
    // ===================
    flash.listen('[data-accept-cookies]', 'click', self.acceptAll.bind(this));
    flash.listen('[data-decline-cookies]', 'click', self.declineAll.bind(this));
    flash.listen('[data-settings-cookies]', 'click', self.openSettings.bind(this));
    flash.listen('[data-close-settings-cookies]', 'click', self.closeInterface.bind(this));

    // INIT RESETTING COOKIES
    // =================
    self.filterCookies();
}

/**
 * Init the cleaning/filtering script
 */
cookiesManager.prototype.filterCookies = function() {
    var self = this;

    // Checking if there's browser support for the advanced features to filter cookies in advance
    // instead of creating a setinterval
    if (!Object.getOwnPropertyDescriptor || !Object.defineProperty) { 
        self.browser_support = false; 
    } else {
        self.browser_support = self.cookies_desc && self.cookies_desc.get && self.cookies_desc.set && typeof self.cookies_desc.get === 'function' && typeof self.cookies_desc.set === 'function' && self.cookies_desc.configurable;
    }
    
    // Temporary setting normal behaviour for the cookies setting
    // in case they were previously changed by this same function
    if(self.browser_support) {
        Object.defineProperty(document, 'cookie', {
            configurable: true,
            enumerable: self.cookies_desc.enumerable,
            get: self.cookies_desc.get,
            set: self.cookies_desc.set
        });
    }

    // Clean up what was already there
    self.deleteAlreadyStoredCookies();

    // Init the filtering of cookies
    if(self.browser_support) {
        // Clean way of filtering new cookies
        Object.defineProperty(document, 'cookie', {
            configurable: true,
            enumerable: self.cookies_desc.enumerable,
            get: function () {
                return self.cookies_desc.get.call(document);
            },
            set: function (cookie) {
                var cookie_name = cookie.substring(0, cookie.indexOf('=')).trim();

                // Checking if the cookie is allowed
                var allow = true;
                var blacklisted_groups = Object.keys(self.user_settings.groups).filter(function(group) {
                    return !self.user_settings.groups[group];
                });
                blacklisted_groups.forEach(function(group_name){
                    var group = self.settings.cookies_groups.find(function(group){return group.name === group_name});
                    if(group.cookies) {
                        if(group.cookies.indexOf(cookie_name) > -1) {
                            allow = false;
                        }
                    }
                })
                // If it's not allowed block it
                if (!allow) {
                    console.log('The cookie ' + cookie_name + ' is not allowed');
                    return;
                }
                // If it's allowed create it normally
                self.cookies_desc.set.call(document, cookie);
            }
        });
    } else {     
        // Fallback to setinterval
        self.setIntervalAndClean();  
    }
}

/**
 * Delete cookies with a setinterval
 */
cookiesManager.prototype.setIntervalAndClean = function() {
    var self = this;

    // Using the setinterval method to remove the cookies
    var tracking_forbidden = false;
    self.settings.cookies_groups.forEach(function(group){
        if(!self.user_settings.groups[group.name]) {
            tracking_forbidden = true;
        }
    });

    if(tracking_forbidden) {
        var groups_to_reset = Object.keys(self.user_settings.groups).filter(function(group) {
            return !self.user_settings.groups[group];
        });

        clearInterval(self.interval);
        self.interval = setInterval(function(){
            groups_to_reset.forEach(function(group_name){
                var group = self.settings.cookies_groups.find(function(group){return group.name === group_name});
                if(group.cookies) {
                    group.cookies.forEach(function(cookie){
                        flash.eraseCookie(cookie);
                    })
                }
            })
        }, 3000);
    } 
}

/**
 * Delete blacklisted cookies
 */
cookiesManager.prototype.deleteAlreadyStoredCookies = function() {
    var self = this;

    var groups_to_reset = Object.keys(self.user_settings.groups).filter(function(group) {
        return !self.user_settings.groups[group];
    });

    groups_to_reset.forEach(function(group_name){
        var group = self.settings.cookies_groups.find(function(group){return group.name === group_name});
        if(group.cookies) {
            group.cookies.forEach(function(cookie){
                flash.eraseCookie(cookie);
            })
        }
    });
}

/**
 * Stores the cookie with the settings
 */
cookiesManager.prototype.updateUserSettings = function() {
    var self = this;

    self.clearAll();
    flash.createCookie(self.settings.cookie_name, JSON.stringify(self.user_settings), 90);
}

/**
 * Accept all the cookies
 */
cookiesManager.prototype.acceptAll = function() {
    var self = this;

    self.settings.cookies_groups.forEach(function(group){
        self.user_settings.groups[group.name] = true;
    });
	self.updateUserSettings();
    self.closeInterface();
}

/**
 * Decline all the cookies
 */
cookiesManager.prototype.groupStatus = function(group_name) {
	var self = this;
    return self.user_settings.groups[group_name];
}

/**
 * Check status of a group
 */
cookiesManager.prototype.declineAll = function() {
	var self = this;

    self.settings.cookies_groups.forEach(function(group){
        self.user_settings.groups[group.name] = false;
    });
    self.clearAll();
	self.updateUserSettings();
    self.closeInterface();
}

/**
 * Check status of a group
 */
cookiesManager.prototype.clearAll = function() {
    var self = this;
    
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

/**
 * Toggles the state of a group of cookies
 */
cookiesManager.prototype.toggleGroup = function(group) {
    var self = this;

    self.user_settings.groups[group] = !self.user_settings.groups[group];
    self.settings_panel.querySelector('[data-toggle-group="' + group + '"]').setAttribute('data-toggle-group-status', self.user_settings.groups[group]);
    self.updateUserSettings();
    self.filterCookies();
}

/**
 * Actions after the settings update
 */
cookiesManager.prototype.closeInterface = function() {
    var self = this;

    // Hiding the bar
    if(self.bar && !self.bar.classList.contains('cookies-bar--hidden')) {
        self.bar.classList.add('cookies-bar--hidden');
    }

    // Hiding the cookies settings
    if(!self.settings_panel.classList.contains('cookies-settings--hidden')) {
        self.settings_panel.classList.add('cookies-settings--hidden');
    }
}

/**
 * Open Settings Bar
 */
cookiesManager.prototype.openSettings = function() {
    var self = this;
    
    // Hiding the cookies settings
    if(self.settings_panel.classList.contains('cookies-settings--hidden')) {
        self.settings_panel.classList.remove('cookies-settings--hidden');

        if(self.user_settings.groups && Object.keys(self.user_settings.groups).length) {
            Object.keys(self.user_settings.groups).forEach(function(group){
                if(self.settings_panel.querySelector('[data-toggle-group="' + group + '"]')) {
                    self.settings_panel.querySelector('[data-toggle-group="' + group + '"]').setAttribute('data-toggle-group-status', self.user_settings.groups[group]);
                }
            });
        }
    }
}