flash.ready(function() {
	function serializeObject(n){var t=function(n,e,r){let a=e||new FormData;for(let e in n){if(!n.hasOwnProperty(e)||!n[e])continue;let o=r?`${r}[${e}]`:e;n[e]instanceof Date?a.append(o,n[e].toISOString()):n[e]instanceof Array?n[e].forEach((n,e)=>{t(n,a,`${o}[${e}]`)}):"object"!=typeof n[e]||n[e]instanceof File?a.append(o,n[e].toString()):t(n[e],a,o)}return a};return new URLSearchParams(t(n)).toString()}
	
	function setupScript($script) {
		var $container = $script.parentNode;
		var search_settings = $script.loadMoreSettings;
		$script.parentNode.removeChild($script);
		var search = function(page) {
			$container.classList.add('loading');
			var endpoint = 'https://vc6hn2s7g5.execute-api.eu-west-1.amazonaws.com/default/flashsearch';
			var search_data = {
			    version: 'draft', // optional, it can be published or draft,
			    token: search_settings.token, // base64 encoded storyblok token,
			    sort_by: search_settings.order_by + ':' + search_settings.sort, // optional, default to position:asc
			    page: page, //optional, default to all of the pages
			    per_page: search_settings.limit, // default to 100
			    conditions: [
			        // these queries use the same syntax as storyblok stories api
			        {
			            field: 'component',
			            operator: 'in',
			            value: search_settings.component
			        }
			    ]
			};
			var xhr = new XMLHttpRequest();
			xhr.open('POST', endpoint, true);
			xhr.send(serializeObject(search_data));
			xhr.onload = function() {
			    $container.classList.remove('loading');
			    var data = JSON.parse(this.responseText);

			    data.stories.forEach(function(story) {
			        $container.innerHTML += search_settings.template(story);
			    });

			    var $link = $container.querySelector('[data-search-load-more]');
			    if($link !== null) {
			        $container.removeChild($link);
			    }
			    $container.innerHTML += search_settings.load_more('data-search-load-more');
			    $link = $container.querySelector('[data-search-load-more]');
			    $link.addEventListener('click', function(e) {
			        e.preventDefault();
			        search(page + 1);
			    }, false);

			    if(page === 1 && !data.stories.length) $container.innerHTML = search_settings.no_results();
			    if(page > 1 && !data.stories.length || data.stories.length < search_settings.limit) $container.removeChild($link);
			}
		};
		search(1);
	}

	// Look for scripts to setup
	var $scripts = document.querySelectorAll('script');
	$scripts.forEach(function($script) {
		if($script.loadMoreSettings !== undefined) setupScript($script);
	});
});